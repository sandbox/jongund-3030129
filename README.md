# A11yFirst Plugins for CKEditor

The A11yFirst Editor Enhancements provide CKEditor plug-ins to support authoring of accessible content by supporting the use of well-structured headings, separating block versus inline styling options, providing immediate feedback on poor link text and text alternative for images, encouraging the use of long descriptions for complicated images and providing a built-in help to learn about creating accessible documents.     After installing the A11yFirst Editor Enhancements module and the required sub-modules (e.g. Fake Objects) the Drupal editor configuration toolbar will include the A11yFirst icons.   The A11yFirst plugins for link and image look the same as the default link and image plugins, so to tell the difference you will need to hover over the icon.   The tooltip that appears will give the name of the plugin and in the case of the A11yFirst plugins “a11ylink” and “a11yimage”.

Description of the A11yFirst plug-ins:
* `a11yheading` and `a11ystylescombo` plugins provide features to support proper using of headings (H1-H6) and separate block styling options from inline styling options in the toolbar.  These plugins are designed to replace the Format and Styles combobox plug-ins in the default editor configuration.
* `a11ylink` plugin provides immediate validation of empty of poor display text for links.  The a11ylink plugin is designed to replace the link plugin in the default editor configuration.
* `a11yimage` plugin provides immediate validation on empty or poor alternative text and provide a means to indicate where a detailed description is located within the document containing the image.  The a11yimage plugin is designed to replace the image plugin used in the default editor configuration. Note: This plugin currently only supports entering image URLs, file browsing for an image is not supported at this time.
* `a11yhelp` plugin provides information on creating accessible documents.

## Required Components
The FakeObject components are used by the a11yimage plugin.

* Install [Drupal CKEditor FakeObject module](https://www.drupal.org/project/fakeobjects)
* Install the [CKEditor FakeObject](https://ckeditor.com/cke4/addon/fakeobjects) object in the `$DRUPAL\libraries` directory

## Installation
1. Make sure required components are installed (e.g. FakeObjects) for a11yimage plugin
1. Create a folder “a11yfirst” in your contributed modules directory “$DRUPAL/modules”
1. Change directory into the a11yfirst directory
1. Use git to install a11yfirst enhancements, using: `git clone https://git.drupal.org/sandbox/jongund/3030129.git`
1. Go to the “Extend” tab in the Drupal Admin interface and enable:
    * A11yFirst Editor Enhancements
    * FakeObjects
1. Go to the “Configuration” tab and select the “Text formats and editors”
1. The Basic and Full editors should now have the A11yFirst icons for the 5 plugins in the configuration toolbar.  You can check this by hovering your mouse over the icons and the A11yFirst plugins will all start with “a11y”.

## Configuring the Basic and Full Editors
There is a two-step process for configuring the Basic and Full editors.   The first step is removing the default icons for adding heading, inline styling and links.   The a11yimage plug-in does not currently have file browsing capabilities., you can only reference images using a URL.  So depending on your needs for referencing images you may not be able to use the A11yImage plugin.
The second step is to include a11y plugins by moving them into the Basic and Full editors, including adding the A11yFirst Help plugins.


## Developer Notes

### Creating tar.gz file command

Go to the a11yfirst module directory and issue the following command:

```
tar -czvf a11yfirst-drupal-module.tar.gz .
```

